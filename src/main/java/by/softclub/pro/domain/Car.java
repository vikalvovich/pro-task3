package by.softclub.pro.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Car implements Comparable<Car> {
    private int id;
    private String name;
    private BigDecimal price;
    private String imagePath;
    private boolean isInBasket;

    public Car(int id) {
        this.id = id;
    }

    @Override
    public int compareTo(Car c) {
        return (this.price.subtract(c.getPrice())).intValue();
    }
}
