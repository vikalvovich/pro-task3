package by.softclub.pro.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person implements Comparable<Person> {
    private int id;
    private String name;
    private int age;
    private String imageName;

    public Person(int id) {
        this.id = id;
        this.imageName = String.format("/images/%d.png", id);
    }

    @Override
    public int compareTo(Person o) {
        return (int)(this.age - o.getAge());
    }
}
