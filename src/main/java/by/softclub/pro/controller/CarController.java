package by.softclub.pro.controller;

import by.softclub.pro.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class CarController {
    @Autowired
    private CarService carService;

    @GetMapping("/carlist")
    public String getCarList(Model model) {
        model.addAttribute("cars", carService.getCarList());
        return "carlist";
    }

    @GetMapping("/basket")
    public String getBasketList(Model model) {
        model.addAttribute("cars", carService.getBasketList());
        return "basket";
    }

    @GetMapping("/history")
    public String getHistoryList(Model model) {
        model.addAttribute("cars", carService.getBuyedCarList());
        return "history";
    }

    @PostMapping("/carlist")
    public String addCarToBasket(@RequestParam("car_id") String car_id, Model model) {
        carService.addToBasked(Integer.valueOf(car_id));
        model.addAttribute("cars", carService.getCarList());
        return "carlist";
    }

    @PostMapping("/basket/delete")
    public RedirectView deleteCar(@RequestParam("car_id") String car_id, Model model) {
        carService.deleteFromBasked(Integer.valueOf(car_id));
        model.addAttribute("cars", carService.getBasketList());
        return new RedirectView("/basket");
    }

    @PostMapping("/basket/buy")
    public RedirectView buyCar(@RequestParam("car_id") String car_id, Model model) {
        carService.buy(Integer.valueOf(car_id));
        model.addAttribute("cars", carService.getBasketList());
        return new RedirectView("/basket");
    }
}
