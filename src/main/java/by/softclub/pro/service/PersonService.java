package by.softclub.pro.service;

import by.softclub.pro.domain.Person;

import java.util.List;

public interface PersonService {
    List<Person> getList();
    void add(Person person);

    int getNextId();
}
