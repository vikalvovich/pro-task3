package by.softclub.pro.service;

import by.softclub.pro.domain.Person;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    private final List<Person> list = new ArrayList<>();

    @PostConstruct
    public void init() {
        list.add(new Person(1, "John", 32, "/images/1.png"));
        list.add(new Person(2, "Alex", 22, "/images/2.png"));
    }

    @Override
    public List<Person> getList() {
        return this.list;
    }

    @Override
    public void add(Person person) {
        list.add(person);
        Collections.sort(list);
    }

    public int getNextId() {
        Person p = Collections.max(list, new Comparator<Person>() {
            @Override
            public int compare(Person first, Person second) {
                return (int) (first.getId() - second.getId());
            }
        });
        return p.getId() + 1;
    }
}
