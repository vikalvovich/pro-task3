package by.softclub.pro.service;

import by.softclub.pro.domain.Car;

import java.util.List;

public interface CarService {
    List<Car> getCarList();
    List<Car> getBasketList();
    List<Car> getBuyedCarList();

    void addToBasked(int id);
    void buy(int id);
    void deleteFromBasked(int id);

}
