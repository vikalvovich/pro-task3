package by.softclub.pro.service;

import by.softclub.pro.domain.Car;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {

    private final List<Car> list = new ArrayList<>();
    private final List<Car> buyedCarList = new ArrayList<>();

    @PostConstruct
    public void init() {
        list.add(new Car(1, "BMW", new BigDecimal(20), "/images/bmw.jpg", false));
        list.add(new Car(2, "OPEL", new BigDecimal(10), "/images/opel.jpg", false));
        list.add(new Car(3, "FORD", new BigDecimal(30), "/images/ford.jpg", false));
        list.add(new Car(4, "CHEVROLET", new BigDecimal(25), "/images/chevrolet.jpg", false));
    }

    @Override
    public List<Car> getCarList() {
        List<Car> collection = list.stream()
                .filter(c -> !c.isInBasket())
                .collect(Collectors.toList());
        Collections.sort(collection);
        return collection;
    }

    @Override
    public List<Car> getBasketList() {
        List<Car> collection = list.stream()
                .filter(c -> c.isInBasket())
                .collect(Collectors.toList());
        Collections.sort(collection);
        return collection;
    }

    @Override
    public List<Car> getBuyedCarList() {
        Collections.sort(buyedCarList);
        return buyedCarList;
    }

    @Override
    public void addToBasked(int id) {
        list.stream()
                .filter(c -> c.getId() == id)
                .forEach(c -> c.setInBasket(true));
    }

    @Override
    public void deleteFromBasked(int id) {
        list.stream()
                .filter(c -> c.getId() == id)
                .forEach(c -> c.setInBasket(false));
    }

    @Override
    public void buy(int id) {
        list.stream()
                .filter(c -> c.getId() == id)
                .forEach(c -> {c.setInBasket(false); buyedCarList.add(c);});
    }

}
